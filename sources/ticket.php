
<?php
  $user = 'a01449747';
  $pass = 'z,kjrj(88)';
  $database = 'lab';

  // establish database connection
  $conn = oci_connect($user, $pass, $database);
  if (!$conn) exit;
    //Handle insert
  if (isset($_GET['BesucherSV'])) {
    //Prepare insert statementd
    $sql = "INSERT INTO Ticket (preis, besucher_sv_nr) VALUES (" . $_GET['Preis'] . "," . $_GET['BesucherSV'] . ")";
    //Parse and execute statement
    $insert = oci_parse($conn, $sql);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    if (!$conn_err & !$insert_err) {
      header("Refresh:0; url=ticket.php");
    }
  }

  // check if search view of list view
  if (isset($_GET['search_preis']) && ($_GET['search_preis'] != '')) {
    $sql = "SELECT * FROM Ticket WHERE preis like '%" . $_GET['search_preis'] . "%'";
  }
  elseif (isset($_GET['besucher_sv']) && ($_GET['besucher_sv'] != '')) {
    $sql = "SELECT * FROM Ticket WHERE besucher_sv_nr like '" . $_GET['besucher_sv'] . "'";
  }
  else {
    $sql = "SELECT * FROM Ticket";
  }

  // execute sql statement
  $stmt = oci_parse($conn, $sql);
  oci_execute($stmt);


  // clean up connections
  oci_free_statement($sproc);
  oci_close($conn);
?>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Alle Tiergärten <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilung.php">Alle Abteilunge</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="mitarbeiter.php">Alle Mitarbeiter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tierbetreuer.php">Alle Tierbetreuer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tier.php">Alle Tiere</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gehege.php">Alle Gehegen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilungsleiter.php">Alle Abteilungsleter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="kassierer.php">Alle Kassierer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="besucher.php">Alle Besucher</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="ticket.php">Alle Tickets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="verkauft.php">Alles was verkauft ist</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <br>

    <div class="row pt-3">
      <div class="col-12">
        <form id='searchform' action='ticket.php' method='get'>
          <div class="input-group">
            <input id='search_preis' style="width: 270px;" name='search_preis' type='text' size='20' placeholder="Suche nach Preis" value='<?php echo $_GET['search_preis']; ?>' />
            <div class="input-group-append" style="margin-right: 30px;">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
            <input id='besucher_sv' style="width: 270px;" name='besucher_sv' type='text' size='20' placeholder="Suche nach Besucher Sv No." value='<?php echo $_GET['besucher_sv']; ?>' />
            <div class="input-group-append">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <br>

    <?php
      // check if search view of list view
      if (isset($_GET['search_preis']) && ($_GET['search_preis'] != '')) {
        $sql = "SELECT * FROM Ticket WHERE preis like '%" . $_GET['search_preis'] . "%'";
      }
      elseif (isset($_GET['besucher_sv']) && ($_GET['besucher_sv'] != '')) {
        $sql = "SELECT * FROM Ticket WHERE besucher_sv_nr like '" . $_GET['besucher_sv'] . "'";
      }
      else {
        $sql = "SELECT * FROM Ticket";
      }

      // execute sql statement
      $stmt = oci_parse($conn, $sql);
      oci_execute($stmt);
    ?>

    <div class="row">
      <div class="col-12" style="height: 460px; overflow-y: scroll;">
        <table class="table "style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;">
            <tr>
              <th>ID </th>
              <th>Preis</th>
              <th>Besucher SVNr</th>
            </tr>
          </thead>
          <tbody>
            <?php
              // fetch rows of the executed sql query
              while ($row = oci_fetch_assoc($stmt)) {
                echo "<tr>";
                echo "<td>" . $row['TICKET_ID'] ."</td>";
                echo "<td>" . $row['PREIS'] ."</td>";
                echo "<td>" . $row['BESUCHER_SV_NR'] ."</td>";
                echo "</tr>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <br>
    <br>
    <div>Insgesamt <?php echo oci_num_rows($stmt); ?> Tickets gefunden!</div>
    <?php  oci_free_statement($stmt); ?>

    <br>
    <br>

    <div>
      <form id='insertform' action='ticket.php' method='get'>
        Neuer Ticket einfuegen:
        <table class="table" style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;">
            <tr>
              <th>Preis</th>
              <th>Busucher SV Nr.</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <input class="form-control" id='Preis' name='Preis' type='text' size='20' value='<?php if (isset($_GET['Preis'])) echo $_GET['Preis']; ?>' />
            </td>
            <td>
              <input class="form-control" id='BesucherSV' name='BesucherSV' type='text' size='20' value='<?php if (isset($_GET['BesucherSV'])) echo $_GET['BesucherSV']; ?>' />
            </td>
          </tr>
          </tbody>
        </table>
        <button class="btn btn-outline-secondary float-right" id='submit' type='submit'> Insert </button>
      </form>
    </div>
    <?php
      if (isset($_GET['BesucherSV'])) {
        if(!$conn_err & !$insert_err) {
          print("Successfully inserted");
          print("<br>");
          header("Refresh:0; url=ticket.php");
        }
        //Print potential errors and warnings
        else{
            print($conn_err);
            print_r($insert_err);
            print("<br>");
        }
      }
      oci_free_statement($insert);
    ?>
  </div>
</body>
</html>
