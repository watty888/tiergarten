<?php
  $user = 'a01449747';
  $pass = 'z,kjrj(88)';
  $database = 'lab';

  // establish database connection
  $conn = oci_connect($user, $pass, $database);
  if (!$conn) exit;

  // check if search view of list view
  if (isset($_GET['gehegeNr']) && ($_GET['abt_nr'] != '')) {
    $sql = "SELECT * FROM Gehege WHERE gehege_nr like " . $_GET['gehegeNr'] . "";
  }
  elseif (isset($_GET['abt_nr']) && ($_GET['abt_nr'] != '')) {
    $sql = "SELECT * FROM Gehege WHERE abteilungs_nr like " . $_GET['abt_nr'] . "";
  }
  elseif (isset($_GET['tier_art']) && ($_GET['tier_art'] != '')) {
    $sql = "SELECT * FROM Gehege WHERE tierart like '%" . $_GET['tier_art'] . "%'";
  }
  elseif (isset($_GET['Groesse']) && ($_GET['Groesse'] != '')) {
    $sql = "SELECT * FROM Gehege WHERE groesse like " . $_GET['Groesse'] . "";
  }
  else {
    $sql = "SELECT * FROM Gehege";
  }

  // execute sql statement
  $stmt = oci_parse($conn, $sql);
  oci_execute($stmt);

  //Handle insert
  if (isset($_GET['abtNo']))
  {
    //Prepare insert statementd
    $sql = "INSERT INTO Gehege (abteilungs_nr, tierart, groesse) VALUES (" . $_GET['abtNo'] . ",'" . $_GET['tierArt'] . "'," . $_GET['grs'] . ")";
    //Parse and execute statement
    $insert = oci_parse($conn, $sql);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    $sqlUpdateCages = "UPDATE Tierabteilung SET gehegen_anz = gehegen_anz + 1 WHERE abteilungs_nr = " . $_GET['abtNo'] . "";
    echo "$sqlUpdateCages";
    $execUpdateCages = oci_parse($conn, $sqlUpdateCages);
    oci_execute($execUpdateCages);

    if(!$conn_err & !$insert_err) {
      header("Refresh:1; url=gehege.php");
    }
  }
  // clean up connections
  oci_free_statement($sproc);
  oci_close($conn);
?>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
</head>

  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="index.php">Alle Tiergärten <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="abteilung.php">Alle Abteilunge</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="mitarbeiter.php">Alle Mitarbeiter</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="tierbetreuer.php">Alle Tierbetreuer</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="tier.php">Alle Tiere</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="gehege.php">Alle Gehegen</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="abteilungsleiter.php">Alle Abteilungsleiter</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="kassierer.php">Alle Kassierer</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="besucher.php">Alle Besucher</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="ticket.php">Alle Tickets</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="verkauft.php">Alles was verkauft ist</a>
                </li>
              </ul>
            </div>
        </div>
      </div>

      <br>

      <div class="row pt-3">
        <div class="col-12">
          <form id='searchform' action='gehege.php' method='get'>
            <div class="input-group">
              <input id='gehegeNr' style="width: 300px;" name='gehegeNr' type='text' size='20' placeholder="Suche nach Gehegenummer" value='<?php echo $_GET['gehegeNr']; ?>' />
              <div class="input-group-append" style="margin-right: 30px;">
                <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
              </div>
              <input id='abt_nr' style="width: 300px;" name='abt_nr' type='text' size='20' placeholder="Suche nach Abteilungsnummer" value='<?php echo $_GET['abt_nr']; ?>' />
              <div class="input-group-append" style="margin-right: 30px;">
                <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
              </div>
              <input id='tier_art' style="width: 300px;" name='tier_art' type='text' size='20' placeholder="Suche nach Tierart" value='<?php echo $_GET['tier_art']; ?>' />
              <div class="input-group-append" style="margin-right: 30px;">
                <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
              </div>
              <input id='Groesse' style="width: 300px;" name='Groesse' type='text' size='20' placeholder="Suche nach Groesse" value='<?php echo $_GET['Groesse']; ?>' />
              <div class="input-group-append" style="margin-right: 30px;">
                <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-12" style="height: 460px; overflow-y: scroll">
          <table class="table">
            <thead class="thead" style="background-color: #343a40; color: lightgray;">
              <tr>
                <th>Gehegenummer</th>
                <th>Abteilungsnummer</th>
                <th>Tierart</th>
                <th>Groesse</th>
                <th>Anzahl der Tierer</th>
              </tr>
            </thead>
            <tbody>
              <?php
              // fetch rows of the executed sql query
              while ($row = oci_fetch_assoc($stmt)) {
                echo "<tr>";
                echo "<td>" . $row['GEHEGE_NR'] ."</td>";
                echo "<td>" . $row['ABTEILUNGS_NR'] ."</td>";
                echo "<td>" . $row['TIERART'] ."</td>";
                echo "<td>" . $row['GROESSE'] ."</td>";
                echo "<td>" . $row['TIER_ANZAHL'] ."</td>";
                echo "</tr>";
              }
            ?>
            </tbody>
          </table>
        </div>
      </div>

      <br>
      <br>

      <div>Insgesamt <?php echo oci_num_rows($stmt); ?> Gehegen gefunden!</div>
      <?php  oci_free_statement($stmt); ?>

      <br>
      <br>

      <div>
        <form id='insertform' action='gehege.php' method='get'>
          Neues Gehege einfuegen:
          <table class="table" style='border: 1px solid #DDDDDD'>
            <thead class="thead" style="background-color: #343a40; color: lightgray;">
              <tr>
                <th>Abteilungsnummer</th>
                <th>Tierart</th>
                <th>Groesse</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input class="form-control" id='abtNo' name='abtNo' type='text' size='20' value='<?php if (isset($_GET['abtNo'])) echo $_GET['abtNo']; ?>' />
                </td>
                <td>
                  <input class="form-control" id='tierArt' name='tierArt' type='text' size='20' value='<?php if (isset($_GET['tierArt'])) echo $_GET['tierArt']; ?>' />
                </td>
                <td>
                  <input class="form-control" id='grs' name='grs' type='text' size='20' value='<?php if (isset($_GET['grs'])) echo $_GET['grs']; ?>' />
                </td>
              </tr>
            </tbody>
          </table>
          <?php
            if (isset($_GET['abtNo'])) {
              if(!$conn_err & !$insert_err) {
                print("Successfully inserted");
                print("<br>");
                header("Refresh:0; url=abteilung.php");
              }
              //Print potential errors and warnings
              else{
                  print($conn_err);
                  print_r($insert_err);
                  print("<br>");
              }
            }
            oci_free_statement($insert);
          ?>
          <button class="btn btn-outline-secondary float-right" id='submit' type='submit'> Insert </button>
        </form>
      </div>
    </div>
  </body>
</html>