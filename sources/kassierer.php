
<?php
  $user = 'a01449747';
  $pass = 'z,kjrj(88)';
  $database = 'lab';
  // establish database connection
  $conn = oci_connect($user, $pass, $database);
  if (!$conn) exit;

  //Handle insert
  if (isset($_GET['Svnr'])) {
    //Prepare insert statementd
    $sqlMitarbeiter = "INSERT INTO Mitarbeiter (vorname, nachname, sv_nr) VALUES ('" . $_GET['Vorname'] . "','" . $_GET['Nachname'] . "'," . $_GET['Svnr'] . ")";
    $insert = oci_parse($conn, $sqlMitarbeiter);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    if (!$conn_err & !$insert_err) {
      header("Refresh:1; url=kassierer.php");
    }

    oci_free_statement($insert);

    $sqlSelectMaId = "SELECT ma_id FROM Mitarbeiter WHERE sv_nr = " . $_GET['Svnr'];
    $stmt = oci_parse($conn, $sqlSelectMaId);
    oci_execute($stmt);
    $maId = null;
    while ($row = oci_fetch_assoc($stmt)) {
      $maId = $row['MA_ID'];
      break;
    }

    $sqlKassierer = "INSERT INTO Kassierer (kassanumer, fax, ma_id) VALUES (" . $_GET['Kassanumer'] . "," . $_GET['Fax'] . ", " . $maId . ")";

    //Parse and execute statement
    $insert = oci_parse($conn, $sqlKassierer);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    if (!$conn_err & !$insert_err) {
      header("Refresh:0; url=kassierer.php");
    }
  }
  // check if search view of list view
  if (isset($_GET['kassa_n']) && ($_GET['kassa_n'] != '')) {
    $sql = "SELECT Kassierer.ma_id, Kassierer.kassanumer, Kassierer.fax,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Kassierer
              INNER JOIN Mitarbeiter
              ON Kassierer.ma_id = Mitarbeiter.ma_id
              WHERE Kassierer.kassanumer like '" . $_GET['kassa_n'] . "'";
  } elseif (isset($_GET['ma_id']) && ($_GET['ma_id'] != '')) {
    $sql = "SELECT Kassierer.ma_id, Kassierer.kassanumer, Kassierer.fax,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Kassierer
              INNER JOIN Mitarbeiter
              ON Kassierer.ma_id = Mitarbeiter.ma_id
              WHERE Kassierer.ma_id like '" . $_GET['ma_id'] . "'";;
  } else {
    $sql = "SELECT Kassierer.ma_id, Kassierer.kassanumer, Kassierer.fax,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Kassierer
              INNER JOIN Mitarbeiter
              ON Kassierer.ma_id = Mitarbeiter.ma_id";
  }

  // execute sql statement
  $stmt = oci_parse($conn, $sql);
  oci_execute($stmt);

  //echo "$stmt"; // DELETE!

  // clean up connections
  oci_free_statement($sproc);
  oci_close($conn);
?>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
</head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Alle Tiergärten <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilung.php">Alle Abteilunge</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="mitarbeiter.php">Alle Mitarbeiter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tierbetreuer.php">Alle Tierbetreuer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tier.php">Alle Tiere</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gehege.php">Alle Gehegen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilungsleiter.php">Alle Abteilungsleter</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kassierer.php">Alle Kassierer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="besucher.php">Alle Besucher</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ticket.php">Alle Tickets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="verkauft.php">Alles was verkauft ist</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <br>

    <div class="row pt-3">
      <div class="col-12">
        <form id='searchform' action='kassierer.php' method='get'>
          <div class="input-group">
            <input id='kassa_n' style="width: 270;" name='kassa_n' type='text' size='20' placeholder="Suche nach Kassanummer" value='<?php echo $_GET['kassa_n']; ?>' />
            <div class="input-group-append" style="margin-right: 30px;">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
            <input id='ma_id' style="width: 270px;" name='ma_id' type='text' size='20' placeholder="Suche nach Mitarbeiter ID" value='<?php echo $_GET['ma_id']; ?>' />
            <div class="input-group-append">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <br>

    <div class="row">
      <div class="col-12" style="height: 460px; overflow-y: scroll;">
        <table class="table" style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;">
            <tr>
              <th>Ma. ID</th>
              <th>Vorname</th>
              <th>Nachname</th>
              <th>Kassa No.</th>
              <th>Fax</th>
            </tr>
          </thead>
          <tbody>
            <?php
              // fetch rows of the executed sql query
              while ($row = oci_fetch_assoc($stmt)) {
                echo "<tr>";
                echo "<td>" . $row['MA_ID'] ."</td>";
                echo "<td>" . $row['VORNAME'] ."</td>";
                echo "<td>" . $row['NACHNAME'] ."</td>";
                echo "<td>" . $row['KASSANUMER'] ."</td>";
                echo "<td>" . $row['FAX'] ."</td>";
                echo "</tr>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <br>
    <br>
    <div>Insgesamt <?php echo oci_num_rows($stmt); ?> Kassierer gefunden!</div>
    <?php  oci_free_statement($stmt); ?>

    <br>
    <br>

    <div>
      <form id='insertform' action='kassierer.php' method='get'>
        Neuer Kassierer einfuegen:
        <table  class="table" style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;" >
            <tr>
              <th>Vorname</th>
              <th>Nachname</th>
              <th>Kassanummer</th>
              <th>Fax</th>
              <th>SV No.</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <input class="form-control" id='Vorname' name='Vorname' type='text' size='20' value='<?php if (isset($_GET['Vorname'])) echo $_GET['Vorname']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Nachname' name='Nachname' type='text' size='20' value='<?php if (isset($_GET['Nachname'])) echo $_GET['Nachname']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Kassanumer' name='Kassanumer' type='text' size='20' value='<?php if (isset($_GET['Kassanumer'])) echo $_GET['Kassanumer']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Fax' name='Fax' type='text' size='20' value='<?php if (isset($_GET['Fax'])) echo $_GET['Fax']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Svnr' name='Svnr' type='text' size='20' value='<?php if (isset($_GET['Svnr'])) echo $_GET['Svnr']; ?>' />
            </td>
          </tr>
          </tbody>
        </table>
        <?php
          if (isset($_GET['Svnr'])) {
            if(!$conn_err & !$insert_err) {
              print("Successfully inserted");
              print("<br>");
              header("Refresh:0; url=kassierer.php");
            }
            //Print potential errors and warnings
            else{
                print($conn_err);
                print_r($insert_err);
                print("<br>");
            }
          }
          oci_free_statement($insert);
        ?>
        <button class="btn btn-outline-secondary float-right" id='submit' type='submit'> Insert </button>
      </form>
    </div>
  </body>
</html>
