
<?php
  $user = 'a01449747';
  $pass = 'z,kjrj(88)';
  $database = 'lab';
  // establish database connection
  $conn = oci_connect($user, $pass, $database);
  if (!$conn) exit;

  //Handle insert
  if (isset($_GET['Svnr']) && isset($_GET['tbSpec'])) {
    //Prepare insert statementd
    $sqlMitarbeiter = "INSERT INTO Mitarbeiter (vorname, nachname, sv_nr) VALUES ('" . $_GET['Vorname'] . "','" . $_GET['Nachname'] . "'," . $_GET['Svnr'] . ")";
    $insert = oci_parse($conn, $sqlMitarbeiter);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    if (!$conn_err & !$insert_err) {
      header("Refresh:1; url=tierbetreuer.php");
    }

    oci_free_statement($insert);

    $sqlSelectMaId = "SELECT ma_id FROM Mitarbeiter WHERE sv_nr = " . $_GET['Svnr'];
    $stmt = oci_parse($conn, $sqlSelectMaId);
    oci_execute($stmt);
    $maId = null;
    while ($row = oci_fetch_assoc($stmt)) {
      $maId = $row['MA_ID'];
      break;
    }

    $sqlTierBetreuer = "INSERT INTO Tierbetreuer (ma_id, spezialisation) VALUES (" . $maId . ", '" . $_GET['tbSpec'] . "')";

    //Parse and execute statement
    $insert = oci_parse($conn, $sqlTierBetreuer);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    $sqlUpdateAnimalsNo = "UPDATE Tierbetreuer SET anzahl_tiere = anzahl_tiere + 1 WHERE ma_id = " . $maId . "";
    $execUpdateTierBetr = oci_parse($conn, $sqlUpdateAnimalsNo);
    oci_execute($execUpdateTierBetr);

    if (!$conn_err & !$insert_err) {
      header("Refresh:0; url=tierbetreuer.php");
    }
  }
  // check if search view of list view
  if (isset($_GET['spec']) && ($_GET['spec'] != '')) {
    $sql = "SELECT Tierbetreuer.ma_id, Tierbetreuer.spezialisation, Tierbetreuer.anzahl_tiere,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Tierbetreuer
              INNER JOIN Mitarbeiter
              ON Tierbetreuer.ma_id = Mitarbeiter.ma_id
              WHERE Tierbetreuer.spezialisation like '" . $_GET['spec'] . "'";
  } elseif (isset($_GET['abtNo']) && ($_GET['abtNo'] != '')) {
    $sql = "SELECT Tierbetreuer.ma_id, Tierbetreuer.spezialisation,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Tierbetreuer
              INNER JOIN Mitarbeiter
              ON Tierbetreuer.ma_id = Mitarbeiter.ma_id
              WHERE Tierbetreuer.spezialisation like '" . $_GET['abtNo'] . "'";
  } elseif (isset($_GET['tierAnz']) && ($_GET['tierAnz'] != '')) {
    $sql = "SELECT Tierbetreuer.ma_id, Tierbetreuer.spezialisation, Tierbetreuer.anzahl_tiere,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Tierbetreuer
              INNER JOIN Mitarbeiter
              ON Tierbetreuer.ma_id = Mitarbeiter.ma_id
              WHERE Tierbetreuer.anzahl_tiere like '" . $_GET['ma_id'] . "'";
  } else {
    $sql = "SELECT Tierbetreuer.ma_id, Tierbetreuer.spezialisation, Tierbetreuer.anzahl_tiere,
                   Mitarbeiter.vorname, Mitarbeiter.nachname FROM Tierbetreuer
              INNER JOIN Mitarbeiter
              ON Tierbetreuer.ma_id = Mitarbeiter.ma_id";
  }

  // execute sql statement
  $stmt = oci_parse($conn, $sql);
  oci_execute($stmt);


  // clean up connections
  oci_free_statement($sproc);
  oci_close($conn);
?>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
</head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Alle Tiergärten <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilung.php">Alle Abteilunge</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="mitarbeiter.php">Alle Mitarbeiter</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="tierbetreuer.php">Alle Tierbetreuer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tier.php">Alle Tiere</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gehege.php">Alle Gehegen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="abteilungsleiter.php">Alle Abteilungsleiter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="kassierer.php">Alle Kassierer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="besucher.php">Alle Besucher</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ticket.php">Alle Tickets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="verkauft.php">Alles was verkauft ist</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <br>

    <div class="row pt-3">
      <div class="col-12">
        <form id='searchform' action='tierbetreuer.php' method='get'>
          <div class="input-group">
            <input id='ma_id' style="width: 270;" name='ma_id' type='text' size='20' placeholder="Suche nach Mitarbeiter ID" value='<?php echo $_GET['ma_id']; ?>' />
            <div class="input-group-append" style="margin-right: 30px;">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
            <input id='spec' style="width: 270px;" name='spec' type='text' size='20' placeholder="Suche nach Spezialisation" value='<?php echo $_GET['spec']; ?>' />
            <div class="input-group-append" style="margin-right: 30px;">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
            <input id='tierAnz' style="width: 270px;" name='tierAnz' type='text' size='20' placeholder="Suche nach Anzal von Tieren" value='<?php echo $_GET['tierAnz']; ?>' />
            <div class="input-group-append">
              <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <br>

    <div class="row">
      <div class="col-12" style="height: 460px; overflow-y: scroll;">
        <table class="table" style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;">
            <tr>
              <th>Ma. ID</th>
              <th>Spezialisation</th>
              <th>Vorname</th>
              <th>Nachname</th>
              <th>Anzal von Tieren</th>
            </tr>
          </thead>
          <tbody>
            <?php
              // fetch rows of the executed sql query
              while ($row = oci_fetch_assoc($stmt)) {
                echo "<tr>";
                echo "<td>" . $row['MA_ID'] ."</td>";
                echo "<td>" . $row['SPEZIALISATION'] ."</td>";
                echo "<td>" . $row['VORNAME'] ."</td>";
                echo "<td>" . $row['NACHNAME'] ."</td>";
                echo "<td>" . $row['ANZAHL_TIERE'] ."</td>";
                echo "</tr>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <br>
    <br>

    <div>Insgesamt <?php echo oci_num_rows($stmt); ?> Tierbetreuer(s) gefunden!</div>
    <?php  oci_free_statement($stmt); ?>

    <br>
    <br>

    <div>
      <form id='insertform' action='tierbetreuer.php' method='get'>
        Neuer Tierbetreuer einfuegen:
        <table  class="table" style='border: 1px solid #DDDDDD'>
          <thead class="thead" style="background-color: #343a40; color: lightgray;" >
            <tr>
              <th>Spezialisation</th>
              <th>Vorname</th>
              <th>Nachname</th>
              <th>Sv Nr</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <input class="form-control" id='tbSpec' name='tbSpec' type='text' size='20' value='<?php if (isset($_GET['tbSpec'])) echo $_GET['tbSpec']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Vorname' name='Vorname' type='text' size='20' value='<?php if (isset($_GET['Vorname'])) echo $_GET['Vorname']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Nachname' name='Nachname' type='text' size='20' value='<?php if (isset($_GET['Nachname'])) echo $_GET['Nachname']; ?>' />
            </td>
            <td>
              <input class="form-control" id='Svnr' name='Svnr' type='text' size='20' value='<?php if (isset($_GET['Svnr'])) echo $_GET['Svnr']; ?>' />
            </td>
          </tr>
          </tbody>
        </table>
        <?php
          if (isset($_GET['tbSpec'])) {
            if(!$conn_err & !$insert_err) {
              print("Successfully inserted");
              print("<br>");
              header("Refresh:0; url=tierbetreuer.php");
            }
            //Print potential errors and warnings
            else{
                print($conn_err);
                print_r($insert_err);
                print("<br>");
            }
          }
          oci_free_statement($insert);
        ?>
        <button class="btn btn-outline-secondary float-right" id='submit' type='submit'> Insert </button>
      </form>
    </div>
  </body>
</html>
