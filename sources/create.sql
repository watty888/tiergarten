-- noinspection SpellCheckingInspectionForFile

CREATE TABLE Tiergarten
(
	betriebsnummer INT GENERATED ALWAYS AS IDENTITY ,
	stadt VARCHAR(30) NOT NULL ,
	strasse VARCHAR(30) NOT NULL ,
	plz INT NOT NULL ,

	CONSTRAINT pktiergarten PRIMARY KEY (betriebsnummer)
)
/

CREATE TABLE Mitarbeiter
(
  ma_id INT GENERATED ALWAYS AS IDENTITY(START with 1 INCREMENT by 1),
  vorname VARCHAR(25) NOT NULL,
  nachname VARCHAR(25) NOT NULL,
  sv_nr INT DEFAULT 0000000 UNIQUE NOT NULL,

  CONSTRAINT pk_mitarbeiter PRIMARY KEY (ma_id)
)
/

CREATE TABLE Tierabteilung
(
	abteilungs_nr INT,
	abt_name VARCHAR(50) NOT NULL,
	gehegen_anz int DEFAULT 0,

	CONSTRAINT pk_tierabteilung PRIMARY KEY (abteilungs_nr)
)
/

CREATE SEQUENCE  abt_seq START WITH 1
/
CREATE OR REPLACE TRIGGER abt_plus
  BEFORE INSERT ON Tierabteilung
  FOR EACH ROW
  BEGIN
		SELECT abt_seq.nextval
		  INTO :NEW.abteilungs_nr
    FROM dual;
	END;
/

CREATE TABLE Abteilungsleiter
(
	 leiter_id INT,
	 abt_nr INT,
	 email VARCHAR(30) NOT NULL,

	 CONSTRAINT pk_abteilungsleiter PRIMARY KEY (leiter_id),

	 CONSTRAINT fk_ma_id_abteilungsleiter FOREIGN KEY (leiter_id)
	   REFERENCES Mitarbeiter(ma_id) ON DELETE CASCADE,
	 CONSTRAINT fk_abteilungs_nr_leiter FOREIGN KEY (abt_nr)
	   REFERENCES Tierabteilung(abteilungs_nr) ON DELETE CASCADE
)
/

CREATE TABLE Gehege
(
  gehege_nr INT GENERATED ALWAYS AS IDENTITY,
	abteilungs_nr INT,
	tierart VARCHAR(30),
	groesse INT DEFAULT 30 CHECK (groesse >= 30) NOT NULL,
	tier_anzahl INT DEFAULT 0,

	CONSTRAINT pk_gehege PRIMARY KEY (gehege_nr),

	CONSTRAINT fk_abteilungs_nr_gehege FOREIGN KEY (abteilungs_nr)
	  REFERENCES Tierabteilung(abteilungs_nr) ON DELETE CASCADE
)
/

CREATE TABLE Tierbetreuer
(
  ma_id INT,
  spezialisation VARCHAR(50) NOT NULL,
  anzahl_tiere INT DEFAULT 0,

  CONSTRAINT pk_tierbetreuer PRIMARY KEY (ma_id),

  CONSTRAINT fk_ma_id_tierbetreuer FOREIGN KEY (ma_id)
    REFERENCES Mitarbeiter(ma_id) ON DELETE CASCADE
)
/

CREATE TABLE Tier
(
  tier_id INT GENERATED ALWAYS AS IDENTITY,
	gehege_nr INT NOT NULL,
	betreuerID INT,
	name VARCHAR(25) UNIQUE NOT NULL,
	art VARCHAR(25) NOT NULL,
	age INT NOT NULL,

	CONSTRAINT pk_tier_id PRIMARY KEY (tier_id),

	CONSTRAINT fk_betreuer_id FOREIGN KEY (betreuerID)
	  REFERENCES Tierbetreuer(ma_id) ON DELETE CASCADE,
	CONSTRAINT fk_gehege_nr_tier FOREIGN KEY (gehege_nr)
	  REFERENCES Gehege(gehege_nr) ON DELETE CASCADE
)
/

CREATE TABLE Kassierer
(
  ma_id INT,
  kassanumer INT UNIQUE NOT NULL,
  fax INT UNIQUE NOT NULL,

	CONSTRAINT pk_kassierer PRIMARY KEY (ma_id),

	CONSTRAINT fk_ma_id_kassierer FOREIGN KEY (ma_id)
	  REFERENCES Mitarbeiter(ma_id) ON DELETE CASCADE
)
/

CREATE TABLE Besucher
(
  sv_nr INT GENERATED ALWAYS AS IDENTITY,
  vorname VARCHAR(40) NOT NULL,
  nachname VARCHAR(40) NOT NULL,
  zahlungsart VARCHAR(10) DEFAULT 'Cash',

  CONSTRAINT pk_besucher PRIMARY KEY (sv_nr)
)
/

CREATE TABLE Ticket
(
	ticket_id INT GENERATED ALWAYS AS IDENTITY,
	preis NUMBER(7, 2) CHECK (preis >= 0) NOT NULL,
	besucher_sv_nr INT NOT NULL,

	CONSTRAINT pk_ticket PRIMARY KEY (ticket_id),

	CONSTRAINT besucher_sv_nr_ticket FOREIGN KEY (besucher_sv_nr)
	  REFERENCES Besucher(sv_nr)
)
/

CREATE TABLE Verkauft
(
	ma_id INT NOT NULL,
	ticket_id INT NOT NULL,
	sv_nr INT NOT NULL,

 CONSTRAINT pk_verkauft PRIMARY KEY (ticket_id, sv_nr),


	CONSTRAINT fk_ma_id_verkauft FOREIGN KEY (ma_id)
	  REFERENCES Mitarbeiter(ma_id),
	CONSTRAINT fk_ticket_id_verkauft FOREIGN KEY (ticket_id)
	  REFERENCES Ticket(ticket_id),
	CONSTRAINT fk_sv_nr_verkauft FOREIGN KEY (sv_nr)
	  REFERENCES Besucher(sv_nr)
)
/

--  Count all animals
CREATE VIEW total_animals(Animals) AS
  SELECT COUNT(tier_id)
FROM Tier
/

-- animals assigned to betreuer
CREATE VIEW betreuer (BetreuerID, TierID) AS
  SELECT Tierbetreuer.ma_id, Tier.tier_id FROM Tierbetreuer
		LEFT JOIN Tier ON Tierbetreuer.ma_id = Tier.betreuerID
/

-- betreuers with more than one Tier
CREATE VIEW multiple_animals (BetreuerID, TierAnz) AS
  SELECT Tier.betreuerID, COUNT(Tier.tier_id)
		FROM Tier
		GROUP BY tier_id, Tier.betreuerID
		HAVING COUNT(tier_id) > 1
/