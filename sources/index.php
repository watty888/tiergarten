<?php
  $user = 'a01449747';
  $pass = 'z,kjrj(88)';
  $database = 'lab';

  // establish database connection
  $conn = oci_connect($user, $pass, $database);
  if (!$conn) exit;

  // check if search view of list view
  if (isset($_GET['search']) && ($_GET['search']) != '') {
    $sql = "SELECT * FROM Tiergarten WHERE betriebsnummer like '" . $_GET['search'] . "'";
  } else {
    $sql = "SELECT * FROM Tiergarten";
  }

  // execute sql statement
  $stmt = oci_parse($conn, $sql);
  oci_execute($stmt);

  //Handle insert
  if (isset($_GET['Stadt']))
  {
    //Prepare insert statementd
    $sql = "INSERT INTO Tiergarten (stadt, strasse, plz) VALUES ('" . $_GET['Stadt'] . "','" . $_GET['Strasse'] . "','" . $_GET['PLZ'] . "')";
    //Parse and execute statement
    $insert = oci_parse($conn, $sql);
    oci_execute($insert);
    $conn_err=oci_error($conn);
    $insert_err=oci_error($insert);

    if(!$conn_err & !$insert_err) {
      header("Refresh:1; url=index.php");
    }
  }
  // clean up connections
  oci_free_statement($sproc);
  oci_close($conn);
?>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
</head>

  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Alle Tiergärten <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="abteilung.php">Alle Abteilunge</a>
              </li>
                <li class="nav-item">
                  <a class="nav-link" href="mitarbeiter.php">Alle Mitarbeiter</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="tierbetreuer.php">Alle Tierbetreuer</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="tier.php">Alle Tiere</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="gehege.php">Alle Gehegen</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="abteilungsleiter.php">Alle Abteilungsleter</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="kassierer.php">Alle Kassierer</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="besucher.php">Alle Besucher</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="ticket.php">Alle Tickets</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="verkauft.php">Alles was verkauft ist</a>
                </li>
              </ul>
            </div>
        </div>
      </div>

      <br>

      <div class="row pt-3">
        <div class="col-12">
          <form id='searchform' action='index.php' method='get'>
            <div class="input-group">
              <input id='search' style="width: 300px;" name='search' type='text' size='20' placeholder="Suche nach Betriebsnummer" value='<?php echo $_GET['search']; ?>' />
              <div class="input-group-append">
                <button class="btn btn-secondary" id='submit' type='submit'> Los! </button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-12" style="height: 460px; overflow-y: scroll">
          <table class="table">
            <thead class="thead" style="background-color: #343a40; color: lightgray;">
              <tr>
                <th>Btrb. No.</th>
                <th>Stadt</th>
                <th>Straße</th>
                <th>PLZ</th>
              </tr>
            </thead>
            <tbody>
              <?php
              // fetch rows of the executed sql query
              while ($row = oci_fetch_assoc($stmt)) {
                echo "<tr>";
                echo "<td>" . $row['BETRIEBSNUMMER'] ."</td>";
                echo "<td>" . $row['STADT'] ."</td>";
                echo "<td>" . $row['STRASSE'] ."</td>";
                echo "<td>" . $row['PLZ'] ."</td>";
                echo "</tr>";
              }
            ?>
            </tbody>
          </table>
        </div>
      </div>
      <div>Insgesamt
        <?php echo oci_num_rows($stmt); ?> Tiergärten gefunden!</div>
      <?php  oci_free_statement($stmt); ?>

      <br>
      <br>

      <div>
        <form id='insertform' action='index.php' method='get'>
          Neuer Tiergarten einfuegen:
          <table class="table" style='border: 1px solid #DDDDDD'>
            <thead class="thead" style="background-color: #343a40; color: lightgray;">
              <tr>
                <th>Stadt</th>
                <th>Straße</th>
                <th>PLZ</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input class="form-control" id='Stadt' name='Stadt' type='text' size='20' value='<?php if (isset($_GET['
                    Stadt'])) echo $_GET['Stadt']; ?>' />
                </td>
                <td>
                  <input class="form-control" id='Strasse' name='Strasse' type='text' size='20' value='<?php if (isset($_GET['
                    Strasse'])) echo $_GET['Strasse']; ?>' />
                </td>
                <td>
                  <input class="form-control" id='PLZ' name='PLZ' type='text' size='20' value='<?php if (isset($_GET['
                    PLZ'])) echo $_GET['PLZ']; ?>' />
                </td>
              </tr>
            </tbody>
          </table>
          <?php
            if (isset($_GET['Stadt'])) {
              if(!$conn_err & !$insert_err) {
                print("Successfully inserted");
                print("<br>");
                header("Refresh:0; url=index.php");
              }
              //Print potential errors and warnings
              else{
                  print($conn_err);
                  print_r($insert_err);
                  print("<br>");
              }
            }
            oci_free_statement($insert);
          ?>
          <button class="btn btn-outline-secondary float-right" id='submit' type='submit'> Insert </button>
        </form>
      </div>
    </div>
  </body>
</html>